module mind-framework/asterisk/eagi-voskserver

go 1.15

require (
	github.com/mind-ar/goEagi v0.2.1-mind
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f
)
