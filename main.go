package main

import (
	"fmt"
	"math"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/mind-ar/goEagi"
	"golang.org/x/net/context"
)

func main() {

	eagi, err := goEagi.New()
	if err != nil {
		eagi.Verbose(err.Error(), 1)
		return
	}

	voskHost := os.Getenv("VOSKSERVER_HOST")
	voskPort := os.Getenv("VOSKSERVER_PORT")
	if voskHost == "" {
		eagi.Verbose("error al conectar a VOSK: VOSKSERVER_HOST no definido", 1)
		return
	}
	if voskPort == "" {
		voskPort = "2700"
	}

	eagi.Verbose("Conectando a "+voskHost+":"+voskPort, 1)

	var phrases = eagi.Env["arg_1"]
	minScore, err := strconv.Atoi(eagi.Env["arg_2"])
	if err != nil {
		minScore = 100
	}
	var detectedScore = 0

	var phraseList = strings.Split(phrases, ",")
	var partial string

	eagi.Verbose(phraseList, 1)

	voskService, err := goEagi.NewVoskService(voskHost, voskPort, phraseList)
	if err != nil {
		eagi.Verbose(err.Error(), 1)
		return
	}
	defer voskService.Close()

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*60))
	defer cancel()
	silenceTimeout := time.Second * 5
	silenceTimeoutch := time.After(silenceTimeout)

	bridgeStream := make(chan []byte)
	defer close(bridgeStream)

	audioStream := goEagi.AudioStreaming(ctx)
	errStream := voskService.StartStreaming(ctx, bridgeStream)
	voskStream := voskService.SpeechToTextResponse(ctx)

	vad := goEagi.NewVad()
	vad.AmplitudeDetectionThreshold = -24
	cancelVad := make(chan interface{})
	vadStream := vad.Detect(cancelVad, bridgeStream)

	audioStarted := false

	sigs := make(chan os.Signal)
	signal.Notify(sigs, syscall.SIGHUP)
loop:
	for {
		select {
		case <-sigs:
			eagi.Verbose("SIGHUP", 1)
			voskService.Close()
			// break loop

		case <-ctx.Done():
			eagi.Verbose("TIMEOUT", 1)
			voskService.Close()
			// break loop

		case <-vadStream:
			silenceTimeoutch = time.After(silenceTimeout)

		case <-silenceTimeoutch:
			eagi.Verbose("SILENCE TIMEOUT", 1)
			voskService.Close()
			// break loop

		case err := <-errStream:
			eagi.Verbose(err.Error(), 1)
			break loop

		case a := <-audioStream:
			if a.Error != nil {
				eagi.Verbose(a.Error.Error(), 1)
				break loop
			}

			if len(a.Stream) != 0 {
				bridgeStream <- a.Stream
			}
			if !audioStarted {
				audioStarted = true
				eagi.Verbose("Audio Detectado", 1)
			}

		case v := <-voskStream:
			if v.Text != "" {
				eagi.Verbose(fmt.Sprintf("Text: %s \n Score: %d/%d\n", v.Text, detectedScore, minScore), 5)
				eagi.SetVariable("ASR_LASTRESULT", v.Text)
				eagi.SetVariable("ASR_MAXCSORE", detectedScore)
				if detectedScore >= minScore {
					eagi.SetVariable("ASR_RESULT", "Ok")
				}
				break loop
			} else if v.Partial != "" {
				if v.Partial != partial {
					// eagi.Verbose(fmt.Sprintf("\nParcial: %s\n\n", v.Partial), 5)
					partial = v.Partial
					for _, p := range phraseList {
						detectedScore = levenshteinDistance(p, v.Partial)
						eagi.Verbose(fmt.Sprintf("Partial: %s \n Score: %d/%d\n", v.Partial, detectedScore, minScore), 5)
						if detectedScore >= minScore {
							voskService.Close()
						}
					}
				}
			}
		}
	}

}

func levenshteinDistance(s string, t string) int {

	// degenerate cases
	s = strings.ToLower(s)
	t = strings.ToLower(t)
	if s == t {
		return 100
	}
	if len(s) == 0 {
		return 0
	}
	if len(t) == 0 {
		return 0
	}

	maxLength := len(s)
	if len(t) > maxLength {
		maxLength = len(t)
	}

	// create two work vectors of integer distances
	v0 := make([]int, len(t)+1)
	v1 := make([]int, len(t)+1)

	// initialize v0 (the previous row of distances)
	// this row is A[0][i]: edit distance for an empty s
	// the distance is just the number of characters to delete from t
	for i := 0; i < len(v0); i++ {
		v0[i] = i
	}

	for i := 0; i < len(s); i++ {
		// calculate v1 (current row distances) from the previous row v0

		// first element of v1 is A[i+1][0]
		//   edit distance is delete (i+1) chars from s to match empty t
		v1[0] = i + 1

		// use formula to fill in the rest of the row
		for j := 0; j < len(t); j++ {
			var cost int
			if s[i] == t[j] {
				cost = 0
			} else {
				cost = 1
			}
			v1[j+1] = int(math.Min(float64(v1[j]+1), math.Min(float64(v0[j+1]+1), float64(v0[j]+cost))))
		}

		// copy v1 (current row) to v0 (previous row) for next iteration
		for j := 0; j < len(v0); j++ {
			v0[j] = v1[j]
		}
	}

	distancia := v1[len(t)]
	if distancia == 0 {
		return 100
	}

	score := (float64(distancia) / float64(maxLength)) * 100

	return 100 - int(score)
	// return ((maxLength - distancia) / maxLength) * 100
}
