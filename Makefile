dist:
	go mod vendor
	go build

install: dist
	go install

run: dist
	cd suites/dev/; \
	docker-compose up -d

send-call: install
	cd suites/dev && ./send-call.sh

stop:
	cd suites/dev/; \
	docker-compose down